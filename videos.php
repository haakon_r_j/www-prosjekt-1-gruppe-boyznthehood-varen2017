<?php

	require_once 'header.php';

	$videos; 	// this array will store every video currently relevant. ( those that are being shown on current page)
	$nrOfMatches; // nr of videos matched.
	$nrOfVideosPerPage = 10; // nr of videos to display on each page.

	if (isset($_POST['searchText'])) {
		session_start();
		$_SESSION['videoSearch'] = $_POST['searchText'];
	}

	$searchText = "";

	if (isset($_SESSION['videoSearch']) && $_SESSION['videoSearch'] != "") {
		$searchText = $_SESSION['videoSearch'];
	}

	$videosData = $videoInterface->getVideosBySearch($searchText, $_GET['page'], $nrOfVideosPerPage);
	$videos = $videosData['videos'];
	$nrOfMatches = $videosData['nrOfVideos'];

?>

<!DOCTYPE html>
<html>
<head>
	<title>videos</title>
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/videos.css">


	</style>

</head>
<body>

<div class="container-fluid contentContainer">
	<div id="searchDiv">
		<form class="form-inline" action="videos.php?page=1" method="post">
		  <div class="form-group">
		    <label for="searchfield" class="sr-only">Search</label>
		    <input type="text" class="form-control" name="searchText" id="searchfield">
		  </div>
		  <button type="submit" class="btn btn-primary">Search</button>
		</form>
	</div>


	<div class="container-fluid" id="resultsContainer">

	<?php
		foreach ($videos as $key => $video) {
			echo "
		<div class='card'>
			<a href='watch.php?videoId=".$video['id']."'>
		  	<div class='card-block'>
		 		<img class='card-img-top' src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960' alt='Card image cap'>
		 		<div class='cardTitleAndDescrptionDiv'>
			 		<h5 class='card-title'>".$video['title']."</h4>
			 		<p>".$video['description']."</p>
		 		</div>
		 	</div>
		 	</a>
		</div>
		";
		}

	?>


	</div>

	<!--Pagenation for search results-->
	<nav aria-label="Page navigation results" id="resultsPagenation">
	  <ul class="pagination">
	    <?php
	    	$prevPage = ($_GET['page'] - 1);
	    	if ($prevPage < 1) {
	    		$prevPage = 1;
	    	}
	   		echo "<li class='page-item'><a class='page-link' href='videos.php?page=".$prevPage."'>Previous</a></li>";

	   		$i = 0;
	    	for ($i; $i < $nrOfMatches / $nrOfVideosPerPage; $i++) {
	    		echo "<li class='page-item nrLink'><a class='page-link' href='videos.php?page=".($i + 1)."'>".($i + 1)."</a></li>";
	    	}
	    	$nextPage = ($_GET['page'] + 1);
	    	if ($nextPage > $i) {
	    		$nextPage = $i;
	    	}
	    	echo "<li class='page-item'><a class='page-link' href='videos.php?page=".$nextPage."'>Next</a></li>";
	    ?>
	  </ul>
	</nav>

</div>

<script src="jquery.sortable.js"></script>

<script type="text/javascript">


</script>


</body>
</html>
