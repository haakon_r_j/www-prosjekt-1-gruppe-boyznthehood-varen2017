<?php
  require_once 'header.php';


   if (isset($_POST['action']) && !empty($_POST['action'])) {
   		if ($_POST['action'] == 'DELETE_USER') {
   			$user->deleteUser($_POST['uid']);
   			echo "deleted user";
   		}
   		else {
   			echo "failed";
   		}

	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>userlist</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/global.css">
  <link rel="stylesheet" href="css/listUsers.css">
</head>
<body>

	<div class="container-fluid contentContainer">
	


<div class="container-fluid" id="resultsContainer">

<table class="table table-striped">
  <thead class="thead-inverse">
    <tr>
      <th>Id</th>
      <th>Email</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  <?php
  	$users = $user->listUsers();
  	foreach ($users as $person) {
  		echo "<tr><th scope='row'>".$person['uid']."</th>";
  		echo '<td>'.$person['email'].'</td>';

  		echo '<td>';
  		if (!$person['admin']) {
  			echo '<i class="fa fa-times" aria-hidden="true"></i>';
  		}
  		echo '</td></tr>';
  	}

  ?>
  </tbody>
</table>

	</div>
	</div>

	<script type="text/javascript" src="js/userList.js">
	</script>

</body>
</html>
