This project is made by:

Haakon Reiss-Jacobsen 141246
Einar Kaspersen Budsted 131348
Martin Holltr� Spongsveen 141238

group boyznthehood.

HOW TO GET STARTED

First make sure to have all the project files downloaded and have a server for
the project and for storing the sql database. Then run the db/installDb.php file
for installing the db to your sql server. This requires you to change the
contents of the variables $servername, $username and $password to your own
server information. These variables is found at the top of the file. When you
run the file on your server it will create a database to the server you
specified for the database. You will see on the screen if the db was created
and if all the tables where created. If there should be something wrong the db
also exist in a sql file that can be imported to your db server. Just remember
to import into an empty database called "boyznthehood" or you will get an error.

There will also be added a admin user to the db.
username: haakon@gmail.com
password: abc123

Final step will be to change the include/db.php file so it will properly connect
to your db server.

You can use this user to log into the website where you can add more users
