	var videoInModalId; // nasty little variable so we can easily get the id of the video we are editing inside the modal.
	var cardTitleInModal; // same as above, used to set the title of the card without updating the page.


	// When we click the edit button we simply toggle the controls
	$("#toggleMyVideosEditBtn").click(function(event) {
		$(".myVideoesDeleteBtn").toggle();
		$(".myVideosEditBtn").toggle();
		$("#deleteSelectedVideosBtn").toggle();
		$(".my-videoes-card").removeClass('deleted-card');
	});


	// When the cross to delete a video is clicked we mark the video for removal by adding the "deleted-card" class.
	// We can then later remove all the marked videos when the big red delete button is clicked.
	$(".myVideoesDeleteBtn").click(function(event) {
		$(this).closest(".my-videoes-card").toggleClass('deleted-card');
	});


	// Deletes all videos marked for removal and have them removed from the database using a ajax call.
	$("#deleteSelectedVideosBtn").click(function(event) {
		var idsToDelete = $('.deleted-card').map(function(){			// gets the ids of every video marked for removal
			var hrefString = $(this).children("a").attr('href');		// stores them in the array idsToDelete
   			return hrefString.substr(hrefString.indexOf("=") + 1);
		}).get();
	
		$.ajax({										// we pass in the array of ids to delete and they will be removed from the server and database.
				context: this,
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'DELETE_VIDEOS', ids: idsToDelete},
			})
			.done(function(data) {
				location.reload();  // reloading here simply cause it is really hard to adjust the layout after removing a video. 
				
			})
			.fail(function() {
				alert("error");
			})
		
	});


	// When the edit icon for a video is clicked we show the modal and load inn the title and description.
	// Any changes done will be saved in the database when the save changes button is clicked and we update the card title.
	$(".myVideosEditBtn").click(function(event) {
		$('#editVideoModal').modal('show');

		cardTitleInModal = $(this).closest('.my-videoes-card').find('.card-title');
		$('#editTitleInput').val(cardTitleInModal.text());			// sets the title text in the modal to the current title.
		$('#editDescriptionInput').val("");

		var hrefString = $(this).closest('.my-videoes-card').find('a').attr('href');
		var videoId = hrefString.substr(hrefString.indexOf("=") + 1);

		videoInModalId = videoId;

		$.ajax({					// ajax call that returns the description of the video as we dont have it stored in here.					
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'GET_VIDEO_DESCRIPTION', id: "" + videoId},
			})
			.done(function(data) {	
				$('#editDescriptionInput').val(data);  	// sets the description text in the modal to the current description.
				
			})
			.fail(function() {
				alert("error");
			});
	});


	// when the save changes button is clicked we take the text from the input fields, and make an ajax call passing in this info
	// along with the id. As a result they will be updated in the database.
	$("#videosEditModalSaveChangesBtn").click(function(event) {
		var newTitle = $('#editTitleInput').val();
		var newDescription = $('#editDescriptionInput').val();

		$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'UPDATE_VIDEO_INFO', id: "" + videoInModalId, title: "" + newTitle, description: "" + newDescription},
			})
			.done(function(data) {	
				$('#editVideoModal').modal('hide');
				$(cardTitleInModal).text(newTitle);
			
				
			})
			.fail(function() {
				alert("error");
			});
	});

	var currentRequest = null; 

	// Set the clicked playlist to active and the other to not active.
	$("#playlistsListDiv li").click(function(event) {
		if ($(this).hasClass('active')) {
			return;
		}	
		$("#playlistsListDiv li").removeClass('active');
		$(this).addClass('active');
		$("#sortPlaylistsDiv ol").empty();
		var videoId = $('#playlistsListDiv .active').attr('data-id');

		
		// we want to abort the previous ajax call if a new one is made, so that e dont accidently load videos from multiple playlists together.
		currentRequest = jQuery.ajax({								
			context: this,					
			url: 'ajax.php',
			type: 'POST',
			data: {action: 'GET_VIDEOS_IN_PLAYLIST', id: "" + videoId},
			dataType: "json",
			beforeSend : function()    {           
		        if(currentRequest != null) {
		            currentRequest.abort();
		        }
	   		},
		})

		// when we succesfully got returned the videos in the list we need to display them, make them sortable and add click listeners for deleting them.
		.done(function(data) {	
			for(var i in data) {
       		  $("#sortPlaylistsDiv ol").append("<li data-id='" + data[i]['id'] + "'><strong>" + data[i]['title'] + "</strong><i class='fa fa-times fa-2x' aria-hidden='true'></i></li>");
    		}
    		$('.sortable').sortable();

    		// whwen we click the X to remove a video form the playlist, we do a ajax call that will remove the entry from the databse.
			$("#sortPlaylistsDiv .fa-times").click(function(event) {
				$.ajax({								
					context: this,					
					url: 'ajax.php',
					type: 'POST',
					data: {action: 'REMOVE_VIDEO_FROM_PLAYLIST', playlistId: "" + videoId, videoId: "" +  $(this).closest('li').attr('data-id')},
				})
				.done(function(data) {	
					// the entry no longer exist in the playlist and we remove it visually.
					$(this).closest('li').toggle();
					
				})
				.fail(function() {
					alert("error");
				});

			});
			
		})
		.fail(function() {
			
		});
	});




	// when the order of the videos ina  list changes, we store that in the database.
	$('.sortable').sortable().bind('sortupdate', function() {

		var idsAndPositions = $("#sortPlaylistsDiv li").map(function(){		
			var id = $(this).attr('data-id');	
			var position = $(this).parent().children().index(this);

			var obj = new Object();
			obj.id = id;
		   	obj.position = position;
		   	var jsonString = JSON.stringify(obj);
			return jsonString;
		}).get();

   		var playlistId = $('#playlistsListDiv .active').attr('data-id');
   		


    	$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'UPDATE_PLAYLIST_VIDEO_ORDER', playlistId: "" + playlistId, idsAndPositions: idsAndPositions},
			})
			.done(function(data) {	
				
				
			})
			.fail(function() {
				alert("error");
		});
	
	});

	// when the + sign in the upload video from is clicked we add a subtitle file field.
	$("#addSubtitleDiv").click(function(event) {
		$("#subtitlesUploadDiv ul").append("<li><input type='file' name='subtitleFiles[]' class='form-control-file' aria-describedby='fileHelp'><select class='form-control' name='languages[]'> <option>English</option><option>Norwegian</option><option>Spanish</option><option>French</option><option>German</option></select></li>");
	});

	// deletes playlist when the X is clicked
	$("#playlistsListDiv .fa-times").click(function(event) {
		
    	$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'DELETE_PLAYLIST', id: "" + $(this).closest('li').attr('data-id')},
			})
			.done(function(data) {	
				$(this).closest('li').toggle();
				
			})
			.fail(function() {
				alert("error");
		});
	
	});


	// some uggly variables wwe use to easily acees the info of the playlist currently in the modal
	var liInPlaylistModal;
	var playlistInModaltitle = "";
	var playlistInModalDescription = "";
	var playlistInModalId;

	// when we click on the edit pencil fo a playlist wwe display the modal and loads in the info.
	$("#playlistsListDiv .fa-pencil").click(function(event) {
		
		$('#editPlaylistModal').modal('show');

		liInPlaylistModal = $(this).closest('li');
		playlistInModaltitle = $(this).closest('li').attr('data-title');
		playlistInModalDescription = $(this).closest('li').attr('data-description');
		playlistInModalId = $(this).closest('li').attr('data-id');
		$('#editPlaylistTitleInput').val(playlistInModaltitle);			
		$('#editPlaylistDescriptionInput').val(playlistInModalDescription);

		
	});

	// wwhen the save chnages button is clicked we save the changed to the database.
	$("#playlistsEditModalSaveChangesBtn").click(function(event) {
		var newTitle = $('#editPlaylistTitleInput').val();
		var newDescription = $('#editPlaylistDescriptionInput').val();
		

		$.ajax({								
				context: this,					
				url: 'ajax.php',
				type: 'POST',
				data: {action: 'UPDATE_PLAYLIST_INFO', id: "" + playlistInModalId, title: "" + newTitle, description: "" + newDescription},
			})
			.done(function(data) {	
				$('#editPlaylistModal').modal('hide');
				$(liInPlaylistModal).find('h5').html(newTitle);

				
			})
			.fail(function() {
				alert("error");
			});
	});

	$("#toggleEditPlaylists").click(function(event) {
		$(".hidden-unless-edit-playlists").toggle();
	});