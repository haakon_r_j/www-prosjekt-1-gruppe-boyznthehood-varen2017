// When we click on a the delete mark next to a person, we make a ajax call to delete the person form the database, and hide the element.
$(".fa-times").click(function(event) {
  var uidToDelete = $(this).closest("tr").children("th").html();
  $.ajax({
    context: this,
    url: 'userList.php',
    type: 'POST',
    data: {action: 'DELETE_USER', uid: "" + uidToDelete},
  })
  .done(function() {
    $(this).closest("tr").toggle();
  })
  .fail(function() {

  })
  .always(function() {

  });

});
