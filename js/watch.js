


    $(".playlist-choice").click(function(event) {

        $.ajax({
                context: this,
                url: 'watch.php',
                type: 'POST',
                data: {action: 'ADD_VIDEO_TO_PLAYLIST', playlistId: "" + $(this).attr('data-playlistId'), videoId: "" + $(this).attr('data-videoId')},
            })
            .done(function() {
                
            })
            .fail(function() {
                alert("error");
            })
            .always(function() {
                
            });

    });

    // playlist js start

  // the playlist is 5 videos in length visually, if we have less videos than that,
  // we wanto to shrink the size of the playlist and diable the arrows as there is nothign to navigate to.
  var nrOfVideosInplaylist = $("#playlistVideosContainer li").length;
  if (nrOfVideosInplaylist < 5) {
    var newContainerwidth = (nrOfVideosInplaylist % 5) * 162 + 2;
    $("#playlistVideosContainer").css('width', "" + newContainerwidth + "px");
    $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
  }

  // we only want to show 5 videos in the list at a time, so to start we hide every video except the 5 first.
  else if (nrOfVideosInplaylist > 5) {
    $("#playlistVideosContainer li:gt(4)").addClass('hidden-video');
  }

  else {
    $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
  }

  // if there is videos in the list, we set the title, src and description etc.
  if (nrOfVideosInplaylist > 0) {
      var firstVideo = $("#playlistVideosContainer li").first();
      loadVideofromPlaylist(firstVideo);
    }

  // the left arrow button will always be diabled at first.
  $("#playlistContainer .fa-arrow-left").addClass('faded-arrow');

  // when a video is clicked it is selected by adding the activeVideoInPlayList. the presvious selected video is
  // had this class removed. We set the source in the video player to play this video. we also set title, description etc.
  $("#playlistVideosContainer li").click(function(event) {
    loadVideofromPlaylist($(this))
  });

  //clicking the right arrow we have to push every video 1 step to the left. we do this by hiding the first visible element, and showing
  //the first hidden element that comes after it.
  $("i.fa-arrow-right").click(function(event) {
    var nextVideo = $("#playlistVideosContainer li:not(.hidden-video)").last().next();
    if (nextVideo.hasClass('hidden-video')) {
      $("#playlistVideosContainer li:not(.hidden-video)").first().addClass('hidden-video');
      nextVideo.removeClass('hidden-video');
      // fades the arrow if there is no more videos to the right.
      if(nextVideo.is(':last-child')) {
        $("#playlistContainer .fa-arrow-right").addClass('faded-arrow');
      }
    }
    if (nrOfVideosInplaylist > 5) {
      $("#playlistContainer .fa-arrow-left").removeClass('faded-arrow');
    }
  });


  // same here but for left button
  $("i.fa-arrow-left").click(function(event) {
    var nextVideo = $("#playlistVideosContainer li:not(.hidden-video)").first().prev();
    if (nextVideo.hasClass('hidden-video')) {
      $("#playlistVideosContainer li:not(.hidden-video)").last().addClass('hidden-video');
      nextVideo.removeClass('hidden-video');

      if(nextVideo.is(':first-child')) {
        $("#playlistContainer .fa-arrow-left").addClass('faded-arrow');
      }
    }
    if (nrOfVideosInplaylist > 5) {
      $("#playlistContainer .fa-arrow-right").removeClass('faded-arrow');
    }
  });


  // when a video finishes we want to play the next in the list if there is one.
  $("#video").bind("ended", function() {
    var nextVideo = $(".activeVideoInPlayList").next('li');
    if (nextVideo.attr('data-id') != null) {
       loadVideofromPlaylist(nextVideo); // if its not the last movie, we load the next.
    }

  });


  // This function takes in a li for a video in the playlist. is sets this video to be the active one
  // and loads the title, description and src etc for the film. we then play the new video
  function loadVideofromPlaylist(videoAsLi) {
    $('#playlistVideosContainer .activeVideoInPlayList').removeClass('activeVideoInPlayList');
    videoAsLi.addClass('activeVideoInPlayList');
    var filePath = videoAsLi.attr('data-filePath');
    $("#video").attr('src', filePath);
    $("h2").html(videoAsLi.attr('data-title'));
		$("#video").attr('data-id', videoAsLi.attr('data-id'))
    $("#descriptionDiv").html(videoAsLi.attr('data-description')); // this one is used to easily add a video to a new playlist

		$("#ratingContainer .fa-star").removeClass('activeStar');
		var videoRating = Math.round(parseInt(videoAsLi.attr('data-rating')));
		$("#ratingContainer .fa-star:lt(" + videoRating + ")").addClass('activeStar');

    loadSubtitleTracksForVideo(videoAsLi.attr('data-id'));

    $("#video")[0].play();


  }







//SUBS START


   $( document ).ready(function() {
  var video = document.getElementById('video');  // Finding the video
  video.play();                                  // Starting the video
  addListeners(video);                       // adding listeners for video events
  setTimeout(function(){
    loadTrack($("video").attr("data-lang"));                           // loading the active text track
  }, 100);

                                             // Changing the text track when a link in
                                             // the dropdown is clicked
  $("#textDiv div#languageSelect > ul > li.available").on('click', 'a' ,changeTrack);
                                             // changing the video time and playing
  $("#textDiv").on('click','p',function(){   // video when a line of thext is clicked
    video_settime($(this).data("start"));
    video.play();
  });


  addLanguageSelectClickListeners();


 $("#speedSelect li").click(function(event) {
    if ($(this).hasClass('active')) {
      return;
    }
    $("#speedSelect li").removeClass('active');
    $(this).addClass('active');
    document.querySelector("#video").playbackRate = $(this).html();
  });



});
// Updating the language
// that is set in the dropdown
// and is giving the new language
// the class active
function changeTrack(){
  event.preventDefault();

  var lang = $('#video').attr("data-lang");

  $("#textDiv div#languageSelect > ul > li.active").each(function(){
    $(this).removeClass("active-track")
  })
  $("div#languageSelect button#languageButton span.lang-sm").attr("data-lang", lang);

  $("#textDiv div#languageSelect > ul > li."+lang).addClass("active");

  loadCurrent();
}


function loadSubtitleTracksForVideo (vidId) {
  $.ajax({
          context: this,
          url: 'ajax.php',
          dataType: "json",
          type: 'POST',
          data: {action: 'GET_TRACK_HTML', videoId: "" + vidId},
      })
      .done(function(data) {

          $("#languageSelect ul").html("");
          $("video").html("");


          $("#video").html(data['trackHtml']);
          $("#languageSelect ul").html(data['languageSelectHtml']);

          $("video").attr("data-lang", "");
          $("video").attr("data-lang", $("#languageSelect li.active").attr("data-lang"));
          
        
          $("#textDiv").html("");
          addLanguageSelectClickListeners();


          loadTrack($("video").attr("data-lang"));

          setTimeout(triggerClick, 200);



      })
      .fail(function() {
          alert("error");
      });
}

function triggerClick() {
  console.log("aa");
  $("#languageSelect li.active").trigger("click");
}


// Adding listeners for when a video is changing time
// and when the vieo is finished
function addListeners(video) {
 video.addEventListener('timeupdate', update, false);
 video.addEventListener('ended', nextvideo, false);
}


function addLanguageSelectClickListeners() {
  $("#languageSelect li").click(function(event) {

    $("#languageSelect li").removeClass('active');
    $(this).addClass('active');
    $("video").attr("data-lang", $(this).attr("data-lang"));
    loadTrack($("video").attr("data-lang"));

  });
}

// Looking for a link in the playlist div with the class next.
// If it is found user will be sent there
function nextvideo(event){
  if($('#playlist a.next').attr('href')){
    window.location.href=$('#playlist a.next').attr('href');
  }
}

// when the video time is changing it will chack if there is a text
// line that should be marked as the current one giving the
// row class hightligth if it is at the right time removing the higlight
// class if it is no longer the correct time
function update(event){
  $('#textDiv').find('p').each(function(index) {
    var row = $(this);
    var start = row.data('start');
    var end =   row.data('end');
    if(parseFloat(start) <= parseFloat(event.target.currentTime) && parseFloat(end) >= parseFloat(event.target.currentTime) ){
      row.addClass('highlight');
    }else{
      row.removeClass('highlight');
    }
  });


  // here we scroll the subtitle text depending on the position relative to the top
  //of the viible textarea, so the highlighted text is always just slightly above the center

  if (!$("#autoplayBtn").hasClass('autoplayBtnOff')) {
    var currentText = $("#textDiv p.highlight");

    if (currentText.length != 0) {
      var curPos = $("#textDiv p.highlight").position().top;

      if (curPos < 240) {
        var currentScroll = $("#textDiv").scrollTop();
        $("#textDiv").animate({ scrollTop: currentScroll + curPos - 240});
      }

      if (curPos > 240) {
        var currentScroll = $("#textDiv").scrollTop();
        console.log(currentScroll);
        $("#textDiv").animate({ scrollTop: currentScroll + curPos - 240});
       }

       else if (curPos < 0) {

       }
     }

  }

}

// decides wwteher autoscroll is on or the user can freely scroll
$("#autoplayBtn").click(function(event) {
    $(this).toggleClass('autoplayBtnOff');
});




// setting the time of the video to a new time
function video_settime(newtime){
  var video = document.getElementsByTagName("video")[0];
  video.currentTime = newtime;
}

// reading the ques from the track and are displaying the textin the textDiv
function readContent(track) {
  $("#textDiv").html("");
  var cues = track.cues;
  for(var i=0; i < cues.length; i++) {
    var cue = cues[i];

    var id    = cue.id ;
    var start = cue.startTime;
    var end   = cue.endTime;
    var text  = cue.text ;
    var rowData = "<p data-start='" + start + "' data-end='" + end + "'>" + text + "</p>"
    $("#textDiv").append(rowData);
  }
}

// getting a video, and loading the video with the language that was
// send as a parameter
function loadTrack(lang) {
  console.log(lang);
  var video = document.getElementById('video');
  for (var i = 0; i < video.textTracks.length; i++) {
    if (video.textTracks[i].language == lang) {
      video.textTracks[i].mode = 'showing';
      readContent(video.textTracks[i]);
    }else {
      video.textTracks[i].mode = 'hidden';
    }
  }

}

$("#ratingContainer .fa-star").click(function(event) {
	$("#ratingContainer .fa-star").removeClass('activeStar');
	$(this).addClass('activeStar');
	var curSelectedStar = $(this).attr('value');
	var videoId = $("#video").attr('data-id');
	$("#ratingContainer .fa-star:lt(" + curSelectedStar + ")").addClass('activeStar');
	$.ajax({
					context: this,
					url: 'ajax.php',
					type: 'POST',
					data: {action: 'RATE_VIDEO', rating: "" + curSelectedStar, videoId: "" + videoId},
			})
			.done(function(data) {

			})
			.fail(function() {
					alert("error");
			})
			.always(function() {
				
			});
});
