
<?php

	require_once 'header.php';

		// ajax result that ads current video to the selected playlist
    if (isset($_POST['action']) && !empty($_POST['action'])) {
        if ($_POST['action'] == 'ADD_VIDEO_TO_PLAYLIST') {
            $videoInterface->addVideoToPlayList($_POST['playlistId'], $_POST['videoId']);
            echo "added video to playlist";
        }
        else {
            echo "failed";
        }

    }

?>


<!DOCTYPE html>
<html>
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/global.css">

  <title>watch</title>
  <link rel="stylesheet" href="css/watch.css">
</head>
<body>
    <div class="container-fluid contentContainer">



     <?php

    	echo "<h2>".$videoInterface->getVideoById($_GET['videoId'])['title']."</h2>";

    ?>


    <div class="container-fluid" id="videoAndTextContainer">

          <?php
					echo "<video id='video' data-id='".$_GET['videoId']."' controls preload='metadata' data-lang='en'>";


            echo  "<source src='".$videoInterface->getVideoById($_GET['videoId'])['filePath']." 'type='video/mp4'>";

            $subtitles = $videoInterface->getSubtitlesByVideoId($_GET['videoId']);
            foreach ($subtitles as $key =>  $subtitle) {
              echo  "<track label='".$subtitle['language']."' kind='subtitles' srclang='".$subtitle['languageCode']."' src='".$subtitle['filePath']."' default>";
            }

            ?>
         </video>

          <div id="textDiv">

          </div>


       <div id='extraControls'>
          <div class="dropdown">
            <a class="btn btn-secondary dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Language
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" id="languageSelect">
              <ul class="list-group">

              <?php
							//gets each subritle for a video
              foreach ($subtitles as $key => $subtitle) {
                  if ($key == 0) {
                    echo  "<li class='list-group-item active' data-lang='".$subtitle['languageCode']."'>".$subtitle['language']."</li>";
                  }
                  else {
                    echo  "<li class='list-group-item' data-lang='".$subtitle['languageCode']."'>".$subtitle['language']."</li>";
                  }
              }

              ?>
              </ul>
            </div>
          </div>

          <div class="dropdown">
            <a class="btn btn-secondary dropdown-toggle" href="https://example.com" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Speed
            </a>

						<!-- lists different items in the select video speed in a dropdown-menu-->
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink2" id="speedSelect">
              <ul class="list-group">
                  <li class="list-group-item">0.25</li>
                  <li class="list-group-item">0.5</li>
                  <li class="list-group-item">0.75</li>
                  <li class="list-group-item active">1</li>
                  <li class="list-group-item">1.25</li>
                  <li class="list-group-item">1.5</li>
                  <li class="list-group-item">1.75</li>
                  <li class="list-group-item">2</li>
                  <li class="list-group-item">2.5</li>
                  <li class="list-group-item">3</li>
              </ul>
            </div>
        </div>
        <div id="addToPlayListContainer">
          <div class="dropdown">
            <a class="btn btn-primary dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Add to playlist
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

            <?php
              if (isset($_SESSION['uid'])) {
								 // get the playlists by user id
                  $playlists = $videoInterface->getPlaylistsByUserId($_SESSION['uid']);
                  foreach ($playlists as $key => $value) {
                      echo "<a class='dropdown-item playlist-choice' data-videoId='".$_GET['videoId']."' data-playlistId='".$value['id']."' href='#'>".$value['title']."</a>";
                  }
              }

            ?>

          </div>
        </div>
      </div>

			<span id="rateItText">Rate it!</span>
			<div id="ratingContainer">
				<?php
				// get the current rating for the video. Value will be users rating of the
				// video, if user has not rated the video the rating will be set to the average
				// rating
				$rating = round($videoInterface->getRating($_GET['videoId']));
					for ($i = 1; $i <= 5; $i++){
						if ($i <= $rating) {
							echo "<i class='fa fa-star activeStar' value='".$i."' aria-hidden='true'></i>";
						}
						else {
							echo "<i class='fa fa-star' value='".$i."' aria-hidden='true'></i>";
						}
					}
				 ?>
			</div>

      <button type="button" class="btn btn-success" id="autoplayBtn">Autoscroll</button>


			</div>

   </div>


      <div id="descriptionDiv">
      <?php
			  //get all the video information
        echo $videoInterface->getVideoById($_GET['videoId'])['description'];
      ?>
      </div>



      <?php
        if (isset($_GET['plId'])) {
          echo "<div class='container-fluid contentContainer'>
                 <div id='playlistContainer'>
                  <i class='fa fa-arrow-left fa-2x' aria-hidden='true'></i>
                  <div id='playlistVideosContainer'>
                    <ul>";

										//get all videos in the selected playlist
                     $videos = $videoInterface->getVideosByPlaylistId($_GET['plId']);
                    foreach ($videos as $key => $video) {
                      if ($key == 0) {
                        echo "<li class='activeVideoInPlayList' data-rating='".$video['rating']."' data-id='".$video['id']."' data-title='".$video['title']."' data-description='".$video['description']."' data-filePath='".$video['filePath']."'><img src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960'><h6>".$video['title']."</h6></li>";
                      }
                      else {
                        echo "<li data-rating='".$video['rating']."' data-id='".$video['id']."' data-title='".$video['title']."' data-description='".$video['description']."' data-filePath='".$video['filePath']."'><img src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960'><h6>".$video['title']."</h6></li>";
                      }
                    }

                echo"</ul>
              </div>
              <i class='fa fa-arrow-right fa-2x' aria-hidden='true'></i>
            </div>

          </div>  ";
        }?>



    </div>

    <script type="text/javascript" src="js/watch.js"></script>

</body>
</html>
