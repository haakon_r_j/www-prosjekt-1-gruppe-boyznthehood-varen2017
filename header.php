<?php
  session_start();
  require_once 'include/db.php';    // Connect to the database
  require_once 'classes/user.php';
  require_once 'classes/videoInterface.php';

  //for quickly adding users

  /*$user->addUser('kdfkdmfdf@hotmail.no', 'kultpw', 'Abelone', 'Bertestad', 91565911, 0);
  $user->addUser('kjkjkjj@hotmail.no', 'fdfdf', 'Mari', 'Anulfsen', 94453452, 0);
  $user->addUser('bnnnbbn@hotmail.no', 'jjhjhj', 'Thomas', 'Arnolfsen', 91457942, 0);
  $user->addUser('kjhgd@hotmail.no', 'lklkl', 'Loke', 'Olsen', 99437222, 1);
  $user->addUser('dddddd@hotmail.no', '3242f3', 'Bjarne', 'Landerud', 98221532, 0);
  $user->addUser('hghgjhj@hotmail.no', 'kultpw', 'Sverre', 'Hagen', 46374855, 0);
  $user->addUser('kags@hotmail.no', 'kultpw', 'Truls', 'Bertulfsen', 92738920, 0);
  $user->addUser('pouuuu@hotmail.no', 'kultpw', 'Maria', 'Bjarnesen', 49982934, 0);
  $user->addUser('kattepus@hotmail.no', 'kultpw', 'Yngve', 'Hattestad', 92837928, 1);
  $user->addUser('kattepus@hotmail.no', 'kultpw', 'Nikoline', 'Tullestad', 98273846, 0);*/

  if(isset($_GET['logout'])) {
    $user->logout();
  }
?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>index</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <link rel="stylesheet" href="css/header.css">

</head>
<body>
  <div class="container-fluid" id="banner">
    <div class="row">
      <h1>Boyznthehood</h1>
    </div>
  </div>



    <nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md navbar-light bg-faded" id="topNavbar">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>


      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
              <a class="nav-link" href="index.php">Home <span class="sr-only"></span></a>
          </li>
            <li class="nav-item">
              <a class="nav-link" href="videos.php?page=1">Videos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="playlists.php?page=1">Playlists</a>
            </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">


          <?php if (isset($_SESSION['uid'])) {  // checks if the user is logged in and show som extra functionality for the users
            echo '<li class="nav-item">
              <a class="nav-link" href="index.php?logout=1">Sign out</a>
          </li>';
          }
          else {
            echo
          '<li role="presentation" class="dropdown" id="signInDropDownToggle">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                Sign in <span class="caret"></span>
              </a>
              <ul class="dropdown-menu dropdown-menu-right">


                <div class="container-fluid" id="signInDropDown">
                  <form method="post" action="index.php?login=1">
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-3 col-form-label signIn-label">Email</label>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-3 col-form-label signIn-label">Password</label>
                      <div class="col-sm-9">
                        <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Password" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class=" col-sm-6">
                        <button type="submit" class="btn btn-primary btn-block">Sign in!</button>
                      </div>
                      <div class="col-sm-6 form-check">
                        <label class="form-check-label">
                          <input class="form-check-input" name="remember" type="checkbox">Remember me
                        </label>
                    </div>

                  </form>';

                  // if login was not successfull;
                   if(isset($_GET['login'])) {
                    echo
                      '<div class="alert alert-danger" role="alert">
                      <strong>Oh snap!</strong> Change a few things up and try submitting again.
                      </div>';
                   }

                  echo
                '</div>
              </ul>
          </li>';
          }
          ?>
      </ul>
      </div>
  </nav>


  <?php if (isset($_SESSION['uid'])) { // checks if the user is logged inn and if he is an admin, if the user has admin rights he is shown
                                       // some extra functionality that are only visible for admins
            echo '<div class="container-fluid" id="side-navbar">
            <div class="row">
                <div class="col-md-4 col-lg-3">

                    <div class="bootstrap-vertical-nav">

                        <button class="btn btn-primary hidden-md-up" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <span class="">Menu</span>
                        </button>

                        <div class="collapse" id="collapseExample">
                            <ul class="nav flex-column" id="exCollapsingNavbar3">
                                <li class="nav-item">
                                    <a class="nav-link" href="profile.php?uid='.$user->uid.'">My profile</a>
                                </li>
                            ';
                                if ($user->getAdmin()) { // checks for admin rights
                                    echo '
                                    <li class="nav-item">
                                        <a class="nav-link" href="userList.php">List Users</a>
                                     </li>
                                     <li class="nav-item">
                                        <a class="nav-link" href="addUser.php">Add User</a>
                                     </li>
                                    ';
                                }
                                echo
                            '</ul>
                        </div>

                    </div>

                </div>

             </div>
          </div>';

    }
    ?>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>







  <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script
      src="https://code.jquery.com/jquery-3.1.1.js"
      integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
      crossorigin="anonymous">
  </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous">
    </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous">
  </script>


  <script type="text/javascript" src="js/header.js">
  </script>

</body>
</html>
