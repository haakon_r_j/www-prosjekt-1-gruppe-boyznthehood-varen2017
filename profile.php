<?php
/**
 * This file shows the different user functions that only logged in users can see
 */

	require_once 'header.php';

	// neww playlist, cxhange this to ajax
	if (isset($_SESSION['uid']) && isset($_POST['title']) && isset($_POST['description']) && $_POST['title'] != "" && $_POST['description'] != "") {
		$videoInterface->createPlaylist($_SESSION['uid'], $_POST['title'], $_POST['description']);
	}
?>

<!DOCTYPE html>
<html>
<head>

	<title>my profile</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/profile.css">
</head>
<body>

<div class="container-fluid contentContainer">


	<button type="button" class="btn btn-warning" id="toggleMyVideosEditBtn">Toggle edit</button>
	<button type="button" class="btn btn-danger" id="deleteSelectedVideosBtn">Delete selected</button>

<div class="container cardsContainer">

<div class='row'>
	<h2><strong>My Videos</strong></h2>
</div>

<div class='row'>
	<p>
  <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#uploadVideoDiv" aria-expanded="false" aria-controls="uploadVideoDiv">
    Upload Video
  </button>
 </div>

 <div class='row'>
	</p>
	<div class="collapse" id="uploadVideoDiv">
	  <form action="upload.php" method="post" enctype="multipart/form-data">
		  <div class="form-group">
		    <label for="newVideotitleInput">Title</label>
		    <input type="text" class="form-control" name="title" id="newVideotitleInput" placeholder="Enter title">
		  </div>
		  <div class="form-group">
		    <label for="newVideoDescriptionInput">Description</label>
		    <input type="text" class="form-control" id="newVideoDescriptionInput" name="description" placeholder="Enter description">
		  </div>
		  <div class="form-group">
		    <label for="newVideoFileInput">Select video to upload</label>
		    <input type="file" class="form-control-file" name="fileToUpload" id="ewVideoFileInput" aria-describedby="fileHelp">
		  </div>
	  	  Subtitles<i class="fa fa-plus" id="addSubtitleDiv" aria-hidden="true"></i>
	  	  <div id=subtitlesUploadDiv>
	  	  	<ul>
	  	  	
	  	  	</ul>
	  	  </div>
	  	  <button type="submit" class="btn btn-primary">Submit</button>

	</form>
	</div>
</div>



<!-- Modal for editing video info-->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editVideoModalTitle" aria-hidden="true" id="editVideoModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editVideoModalTitle">Edit video details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      

		  	<div class="form-group">
		    	<label for="editTitleInput">Title</label>
		    	<input type="text" class="form-control" id="editTitleInput">
		  	</div>
			<div class="form-group">
		   	 	<label for="editDescriptionInput">Description</label>
		    	<textarea class="form-control" id="editDescriptionInput" rows="3"></textarea>
		  	</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="videosEditModalSaveChangesBtn">Save changes</button>
      </div>
    </div>
  </div>
</div>

	
<?php

$videos = $user->getVideos();
foreach ($videos as $key => $value) {

}

echo "<div class='row card-row' data-videoCount='".count($videos)."'><div class='card-deck'>";
$i = 0;
for (; $i < count($videos); $i++) {
	if (!($i % 4) && $i != 0) {
		echo "</div></div><div class='row card-row'><div class='card-deck'>";
	}
	echo "<div class='card my-videoes-card'>
			<div>
				<i class='fa fa-pencil fa-2x myVideosEditBtn' aria-hidden='true'></i><i class='fa fa-times fa-2x myVideoesDeleteBtn' aria-hidden='true'></i>
			</div>
	    	<a href='watch.php?videoId=".$videos[$i]['id']."'><img class='card-img-top' src='https://fbnewsroomus.files.wordpress.com/2015/04/messenger-video-call-carousel-icon.png?w=960' alt='Card image cap'>
	    	<div class='card-block'>
	      		<h5 class='card-title'>".$videos[$i]['title']."</h4>
	    	</div></a>
	  	</div>";
}

for ($j = 4 - $i % 4; $j > 0; $j--) {
	echo "<div class='card hidden-card'>
	    <img class='card-img-top' src='http://www.imgworlds.com/wp-content/uploads/2015/11/img-blvd.jpg' alt='Card image cap'>
	    <div class='card-block'>
	      <h5 class='card-title'>Video title</h4>
	      <p class='card-text'><small class='text-muted'>Added 3 mins ago</small></p>
	    </div>
	  </div>";
}

echo "</div></div>";

?>		


</div>

	<div class="container-fluid cardsContainer">


		<div class='row'>
			<h2><strong>My Playlists</strong></h2>
		</div>

		<div class='row'>
			<p>
		  <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#newPlaylistDiv" aria-expanded="false" aria-controls="newPlaylistDiv">
		    New Playlist
		  </button>
		  </p>
		</div>

		<div class='row'>
			
			<div class="collapse" id="newPlaylistDiv">
			  <form action="profile.php" method="post">
				  <div class="form-group">
				    <label for="newPlaylistTitleInput">Title</label>
				    <input type="text" class="form-control" name="title" id="newPlaylistTitleInput" placeholder="Enter title">
				  </div>
				  <div class="form-group">
				    <label for="newPlaylistDescriptionInput">Description</label>
				    <input type="text" class="form-control" id="newPlaylistDescriptionInput" name="description" placeholder="Enter description">
				  </div>
			  	<button type="submit" class="btn btn-primary">Submit</button>
			  </form>
			</div>
		</div>

		
 <button type="button" class="btn btn-warning" id="toggleEditPlaylists">Toggle edit</button>
		<div class='row'>

			<div id='playlistsListDiv'>
				<ul class='list-group'>
				  <?php 
					  $playlists = $videoInterface->getPlaylistsByUserId($_SESSION['uid']);
					  foreach ($playlists as $playlist) {
					  	echo "<li class='list-group-item' data-id='".$playlist['id']."' data-title='".$playlist['title']."' data-description='".$playlist['description']."'><div><a href='watch.php?plId=".$playlist['id']."'><i class='fa fa-play-circle-o fa-2x' aria-hidden='true'></i></a><h5>".$playlist['title']."</h5></div><i class='fa fa-pencil fa-2x hidden-unless-edit-playlists' aria-hidden='true'></i><i class='fa fa-times fa-2x hidden-unless-edit-playlists' aria-hidden='true'></i></li>";
					  }
				  ?>
				
				</ul>
			</div>

			<div id="sortPlaylistsDiv">
				<ol class='sortable'>

				</ol>
			</div>
			
		</div>

		

	</div>

	<!-- Modal for editing playlist info-->
	<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editPlaylistModalTitle" aria-hidden="true" id="editPlaylistModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="editPlaylistModalTitle">Edit video details</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      

			  	<div class="form-group">
			    	<label for="editPlaylistTitleInput">Title</label>
			    	<input type="text" class="form-control" id="editPlaylistTitleInput">
			  	</div>
				<div class="form-group">
			   	 	<label for="editPlaylistDescriptionInput">Description</label>
			    	<textarea class="form-control" id="editPlaylistDescriptionInput" rows="3"></textarea>
			  	</div>


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="playlistsEditModalSaveChangesBtn">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>


</div>	


<script src="js/jquery.sortable.js"></script>

<script type="text/javascript" src="js/profile.js">
</script>

</body>
</html>
